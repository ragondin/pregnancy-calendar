let date_jour = new Date();

// FONCTION D AFFICHAGE DES DONNEES LORS DU CHARGEMENT INITIAL DE LA PAGE
function param_environnement(){
    // Affichage de la date du jour
    var to_publish = mise_en_forme_date(date_jour);

    // Reinitialisation des dates pickers
    document.getElementById("date_jour").innerHTML = to_publish;
    document.getElementById("date_regles").value = "";
    document.getElementById("date_grossesse").value = "";

} 

// CALCUL DE LA DATE DE LA GROSSESSE SI LA DATE DES REGLES EST RENSEIGNEE
function param_grossesse(param){
    var date_grossesse = new Date(document.getElementById("date_grossesse").value);
    var date_regles = new Date(document.getElementById("date_regles").value);
    if(param ===1){
        date_regles = calcul_date_week(date_grossesse, -2);
        document.getElementById("date_regles").value = mise_en_forme_date(date_regles,"manip");
    }else if(param ===2){
        date_grossesse = calcul_date_week(date_regles, 2);
        document.getElementById("date_grossesse").value = mise_en_forme_date(date_grossesse,"manip");
    }
    /*
    if(isNaN(date_grossesse) == true){
        date_grossesse = calcul_date_week(date_regles, 2);
        document.getElementById("date_grossesse").value = mise_en_forme_date(date_grossesse,"manip");
    }else if (isNaN(date_regles) == true){
        date_regles = calcul_date_week(date_grossesse, -2);
        document.getElementById("date_regles").value = mise_en_forme_date(date_regles,"manip");
    }
    */
    calc_indicateurs(date_grossesse, date_regles);
}

// CALCUL DES INDICATEURS A AFFICHER DANS LE CALENDRIER
function calc_indicateurs(date_grossesse, date_regles){
    // PARTIE DE CALCUL DES DATES
    var date_accouchement = new Date(calcul_date_mois(date_grossesse, 9));

    var age_gestationnel_entier = Math.floor(diff_dates(date_regles, date_jour)/7);
    var age_gestationnel_mod = diff_dates(date_regles, date_jour) % 7;
    if (age_gestationnel_entier === 1){
        var sem_grammaire = " semaine et "
    }else{
        var sem_grammaire = " semaines et "
    }
    if (age_gestationnel_mod <= 1){
        var jour_grammaire = " jour"
    }else{
        var jour_grammaire = " jours"
    }
    var age_gestationnel = age_gestationnel_entier + sem_grammaire + age_gestationnel_mod + jour_grammaire;
    
    //var age_gestationnel = Math.round((diff_dates(date_regles, date_jour)/7)*10)/10;

    var date_limite_declaration = new Date(calcul_date_jour(calcul_date_mois(date_grossesse, 3),-1));

    var date_conge_prenatal = new Date(calcul_date_week(date_accouchement, -holidays_weeks()));

    var date_depistage_trimsomie_t1_deb = new Date(calcul_date_week(date_grossesse,8));
    var date_depistage_trimsomie_t1_fin = new Date(calcul_date_jour(calcul_date_week(date_grossesse,12),-1));
    var date_depistage_trimsomie_t2_deb = new Date(calcul_date_week(date_grossesse,12));
    var date_depistage_trimsomie_t2_fin = new Date(calcul_date_week(date_grossesse,16));

    var echo1 = new Date(calcul_date_week(date_regles, 12));
    var echo1_deb = new Date(calcul_date_week(date_regles, 11));
    var echo1_fin = new Date(calcul_date_jour(calcul_date_week(date_regles, 13),6));
    var echo2 = new Date(calcul_date_week(date_regles, 22));
    var echo2_deb = new Date(calcul_date_week(date_regles, 20));
    var echo2_fin = new Date(calcul_date_week(date_regles, 25));
    var echo3 = new Date(calcul_date_week(date_regles, 32));
    var echo3_deb = new Date(calcul_date_week(date_regles, 30));
    var echo3_fin = new Date(calcul_date_week(date_regles, 35));


    var cs_mois_4_deb = new Date(calcul_date_mois(date_grossesse, 3));
    var cs_mois_4_fin = new Date(calcul_date_jour(calcul_date_mois(date_grossesse, 4),-1));
    var cs_mois_5_deb = new Date(calcul_date_mois(date_grossesse, 4));
    var cs_mois_5_fin = new Date(calcul_date_jour(calcul_date_mois(date_grossesse, 5),-1));
    var cs_mois_6_deb = new Date(calcul_date_mois(date_grossesse, 5));
    var cs_mois_6_fin = new Date(calcul_date_jour(calcul_date_mois(date_grossesse, 6),-1));
    var cs_mois_7_deb = new Date(calcul_date_mois(date_grossesse, 6));
    var cs_mois_7_fin = new Date(calcul_date_jour(calcul_date_mois(date_grossesse, 7),-1));
    var cs_mois_8_deb = new Date(calcul_date_mois(date_grossesse, 7));
    var cs_mois_8_fin = new Date(calcul_date_jour(calcul_date_mois(date_grossesse, 8),-1));
    var cs_mois_9_deb = new Date(calcul_date_mois(date_grossesse, 8));
    var cs_mois_9_fin = new Date(calcul_date_jour(calcul_date_mois(date_grossesse, 9),-1));

    var hgpo_deb = new Date(calcul_date_week(date_regles, 24));
    var hgpo_fin = new Date(calcul_date_week(date_regles, 28));

    // PARTIE AFFICHAGE DES DATES
    document.getElementById("date_accouchement").innerHTML = mise_en_forme_date(date_accouchement);
    document.getElementById("age_gestationnel").innerHTML = age_gestationnel;
    document.getElementById("date_limite_declaration").innerHTML = mise_en_forme_date(date_limite_declaration);
    document.getElementById("date_conge_prenatal").innerHTML = mise_en_forme_date(date_conge_prenatal);

    document.getElementById("echo1").innerHTML = mise_en_forme_date(echo1);
    document.getElementById("echo1_deb").innerHTML = mise_en_forme_date(echo1_deb);
    document.getElementById("echo1_fin").innerHTML = mise_en_forme_date(echo1_fin);
    document.getElementById("echo2").innerHTML = mise_en_forme_date(echo2);
    document.getElementById("echo2_deb").innerHTML = mise_en_forme_date(echo2_deb);
    document.getElementById("echo2_fin").innerHTML = mise_en_forme_date(echo2_fin);
    document.getElementById("echo3").innerHTML = mise_en_forme_date(echo3);
    document.getElementById("echo3_deb").innerHTML = mise_en_forme_date(echo3_deb);
    document.getElementById("echo3_fin").innerHTML = mise_en_forme_date(echo3_fin);

    document.getElementById("cs_mois_4_deb").innerHTML = mise_en_forme_date(cs_mois_4_deb);
    document.getElementById("cs_mois_4_fin").innerHTML = mise_en_forme_date(cs_mois_4_fin);
    document.getElementById("cs_mois_5_deb").innerHTML = mise_en_forme_date(cs_mois_5_deb);
    document.getElementById("cs_mois_5_fin").innerHTML = mise_en_forme_date(cs_mois_5_fin);
    document.getElementById("cs_mois_6_deb").innerHTML = mise_en_forme_date(cs_mois_6_deb);
    document.getElementById("cs_mois_6_fin").innerHTML = mise_en_forme_date(cs_mois_6_fin);
    document.getElementById("cs_mois_7_deb").innerHTML = mise_en_forme_date(cs_mois_7_deb);
    document.getElementById("cs_mois_7_fin").innerHTML = mise_en_forme_date(cs_mois_7_fin);
    document.getElementById("cs_mois_8_deb").innerHTML = mise_en_forme_date(cs_mois_8_deb);
    document.getElementById("cs_mois_8_fin").innerHTML = mise_en_forme_date(cs_mois_8_fin);
    document.getElementById("cs_mois_9_deb").innerHTML = mise_en_forme_date(cs_mois_9_deb);
    document.getElementById("cs_mois_9_fin").innerHTML = mise_en_forme_date(cs_mois_9_fin);
    
    document.getElementById("hgpo_deb").innerHTML = mise_en_forme_date(hgpo_deb);
    document.getElementById("hgpo_fin").innerHTML = mise_en_forme_date(hgpo_fin);
    
    document.getElementById("date_depistage_trimsomie_t1_deb").innerHTML = mise_en_forme_date(date_depistage_trimsomie_t1_deb);
    document.getElementById("date_depistage_trimsomie_t1_fin").innerHTML = mise_en_forme_date(date_depistage_trimsomie_t1_fin);
    document.getElementById("date_depistage_trimsomie_t2_deb").innerHTML = mise_en_forme_date(date_depistage_trimsomie_t2_deb);
    document.getElementById("date_depistage_trimsomie_t2_fin").innerHTML = mise_en_forme_date(date_depistage_trimsomie_t2_fin);


    analyse_des_dates();
}


function analyse_des_dates(){
    var x = document.getElementsByClassName("date_a_checker");
    var i;
    for (i=0; i < x.length; i++){
        date_a_comparer = reverse_date(x[i].innerHTML);

        if(diff_dates(date_a_comparer, date_jour) > 0){
        x[i].classList.add("text-danger");
        x[i].classList.remove("text-white");
        }/*
        else{
            x[i].classList.remove("text-danger");
            x[i].classList.add("text-white");
        }*/
    }
}


// MISE EN FORME DES DATES

function mise_en_forme_date(ugly_date, forme_resultat){
    var month_rework = Number(ugly_date.getMonth())+1;
    if (Number(month_rework) < 10){
        month_rework = String("0") + String(month_rework)
    } else {
        month_rework = String(month_rework)
    }

    var day_rework = Number(ugly_date.getDate());
    if (Number(day_rework) < 10){
        day_rework = String("0") + String(day_rework)
    } else {
        day_rework = String(day_rework)
    }  
    
    if (forme_resultat === "manip"){
         return ugly_date.getFullYear() + '-' + month_rework + '-' + day_rework;
    }else{
        return day_rework + '/' + month_rework + '/' + ugly_date.getFullYear();
    }
   
}

function reverse_date(date_to_reverse){
        var str = String(date_to_reverse);
        var resultat = new Date(str.substr(6,4),str.substr(3,2)-1,str.substr(0,2));
        return resultat;
}
    



// FONCTION DE MANIPULATION DE DATE

function diff_dates(date_deb, date_fin){
    //let date_deb = new Date(date_deb);
    //let date_fin = new Date(date_fin);
    var diff_date = (date_fin - date_deb) / (1000*60*60*24);
    return Math.floor(diff_date);
}


function calcul_date_jour(date1, nb_jours){
    var result = new Date(date1);
    result.setDate(result.getDate()+nb_jours);
    return result;
}


function calcul_date_mois(date1, nb_mois){
    var result = new Date(date1);
    result.setMonth(result.getMonth()+nb_mois);
    return result;
}


function calcul_date_week(date1, nb_weeks){
    var result = new Date(date1);
    result.setDate(result.getDate()+(nb_weeks*7));
    return result;
}

function holidays_weeks(){
    var default_conge = 6;
    return default_conge;
}